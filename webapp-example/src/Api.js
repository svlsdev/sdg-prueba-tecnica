import axios from 'axios'

const BASE_PATH = 'http://localhost:1000/v1/'

const apiPath = (path) => {
  return `${BASE_PATH}${path}`;
}

export default {
  fetchReaders: () => {
    return axios.get(apiPath("readers"));
  },
  sendMsg: (msg) => {
    return axios.post(apiPath("messages"), {msgContent: msg});
  }
}
