import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Publisher from './views/Publisher.vue'
import Reader from './views/Reader.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/publisher',
      name: 'publisher',
      component: Publisher
    },
    {
      path: '/reader',
      name: 'reader',
      component: Reader
    }
  ]
})
