# README #
This repository contains a Spring Boot example for publishing messages and subscribing to them using websockets.

# Requirements
* Java 8
* Maven 3

# Running the server
To start the server simply execute `mvn spring-boot:run` on the root folder. This will start the server on port 1000.

# Running tests
To run all test in project execute `mvn test` on the root folder.

# API usage

## Send messages
To send a new message to all connected message readers send a POST request to `http://localhost:1000/v1/messages` with a JSON body like:
```
{
    "msgContent": "{ message_content }"
}
```



**Curl example**
```
curl -d '{"msgContent":"Test msg"}' \
     -H "Content-Type: application/json"\
     -X POST http://localhost:1000/v1/messages
```

## List message readers
To list currently connected message readers send a GET request to `http://localhost:1000/v1/readers`.

**Curl example**
```
curl http://localhost:1000/v1/readers
```

## Receiving messages
To receive messages it is required a WebSocket client establishing a connection to `ws://localhost:1000/messages`.

**Curl example**
```
curl --include \
     --no-buffer \
     --header "Connection: Upgrade" \
     --header "Upgrade: websocket" \
     --header "Host: localhost:8080" \
     --header "Origin: http://localhost:8080" \
     --header "Sec-WebSocket-Key: SGVsbG8sIHdvcmxkIQ==" \
     --header "Sec-WebSocket-Version: 13" \
     http://localhost:1000/messages
```

**Javascript example**
```
// Connecting to socket
let socket = new WebSocket("ws://localhost:1000/messages")

socket.onmessage = function (event) {
    console.log(JSON.parse(event.data));
}
    
// Disconnecting from socket
socket.close();
```

## Running the webapp example
Under `webapp-example` there is an example webapp for publishing and subscribing.

To run the webapp example execute the following commands on the `/webapp-example` folder:

1. `npm install`
2. `npm run serve`

Once is started open a browser window and navigate to `http://localhost:8080/`.
