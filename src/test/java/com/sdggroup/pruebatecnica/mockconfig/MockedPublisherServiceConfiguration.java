package com.sdggroup.pruebatecnica.mockconfig;

import com.sdggroup.pruebatecnica.api.publisher.PublisherService;
import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class MockedPublisherServiceConfiguration {

    @Bean
    public PublisherService publisherService() {
        return Mockito.mock(PublisherService.class);
    }
}
