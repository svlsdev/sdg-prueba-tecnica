package com.sdggroup.pruebatecnica.mockconfig;

import com.sdggroup.pruebatecnica.api.reader.ReaderService;
import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class MockedReaderServiceConfiguration {

    @Bean
    public ReaderService readerService() {
        return Mockito.mock(ReaderService.class);
    }
}
