package com.sdggroup.pruebatecnica.mockconfig;

import com.sdggroup.pruebatecnica.model.socket.SocketHandler;
import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class MockedSocketHandlerConfiguration {

    @Bean
    public SocketHandler socketHandler() {
        return Mockito.mock(SocketHandler.class);
    }
}
