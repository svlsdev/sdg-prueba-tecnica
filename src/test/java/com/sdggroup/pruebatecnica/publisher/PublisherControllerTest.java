package com.sdggroup.pruebatecnica.publisher;

import com.sdggroup.pruebatecnica.api.publisher.PublisherService;
import com.sdggroup.pruebatecnica.api.publisher.dto.MessageSentDto;
import com.sdggroup.pruebatecnica.api.publisher.dto.SendMessageDto;
import com.sdggroup.pruebatecnica.api.publisher.exception.EmptyMessageException;
import com.sdggroup.pruebatecnica.api.publisher.exception.NullMessageException;
import com.sdggroup.pruebatecnica.mockconfig.MockedPublisherServiceConfiguration;
import com.sdggroup.pruebatecnica.rest.APIErrorResponse;
import com.sdggroup.pruebatecnica.rest.publisher.request.SendMessageRequest;
import com.sdggroup.pruebatecnica.rest.publisher.response.SendMessageResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.time.OffsetDateTime;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = MockedPublisherServiceConfiguration.class)
@ActiveProfiles("test")
public class PublisherControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private PublisherService publisherService;

    @Test
    public void givenValidMsg_whenPublish_thenRespondSent() {

        final String msgContent = "Test msg";
        final OffsetDateTime now = OffsetDateTime.now();

        final SendMessageDto sendMessageDto = new SendMessageDto();
        sendMessageDto.setMsgContent(msgContent);

        final MessageSentDto messageSentDto = new MessageSentDto();
        messageSentDto.setMsgContent(msgContent);
        messageSentDto.setReceivedAt(now);

        Mockito.when(publisherService.publish(sendMessageDto)).thenReturn(messageSentDto);

        final SendMessageRequest messageRequest = new SendMessageRequest();
        messageRequest.setMsgContent(msgContent);

        final SendMessageResponse response;
        response = restTemplate.postForObject("/v1/messages", messageRequest, SendMessageResponse.class);

        Assertions.assertEquals(messageRequest.getMsgContent(), response.getMsgContent());
        Assertions.assertNotNull(response.getReceivedAt());
    }

    @Test
    public void givenNullMsg_whenPublish_thenRespondError() {

        final SendMessageDto sendMessageDto = new SendMessageDto();

        Mockito.when(publisherService.publish(sendMessageDto)).thenThrow(NullMessageException.class);

        final SendMessageRequest messageRequest = new SendMessageRequest();

        final APIErrorResponse response;
        response = restTemplate.postForObject("/v1/messages", messageRequest, APIErrorResponse.class);

        final APIErrorResponse expectedResponse;
        expectedResponse = new APIErrorResponse(400, "Message cannot be null");

        Assertions.assertEquals(response, expectedResponse);
    }

    @Test
    public void givenEmptyMsg_whenPublish_thenRespondError() {

        final SendMessageDto sendMessageDto = new SendMessageDto();
        sendMessageDto.setMsgContent(" ");

        Mockito.when(publisherService.publish(sendMessageDto)).thenThrow(EmptyMessageException.class);

        final SendMessageRequest messageRequest = new SendMessageRequest();
        messageRequest.setMsgContent(" ");

        final APIErrorResponse response;
        response = restTemplate.postForObject("/v1/messages", messageRequest, APIErrorResponse.class);

        final APIErrorResponse expectedResponse;
        expectedResponse = new APIErrorResponse(400, "Message cannot be empty");

        Assertions.assertEquals(response, expectedResponse);
    }
}
