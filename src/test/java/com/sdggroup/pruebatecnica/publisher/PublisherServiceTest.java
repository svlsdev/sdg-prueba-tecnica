package com.sdggroup.pruebatecnica.publisher;

import com.sdggroup.pruebatecnica.api.publisher.dto.MessageSentDto;
import com.sdggroup.pruebatecnica.api.publisher.dto.SendMessageDto;
import com.sdggroup.pruebatecnica.api.publisher.exception.EmptyMessageException;
import com.sdggroup.pruebatecnica.api.publisher.exception.NullMessageException;
import com.sdggroup.pruebatecnica.mockconfig.MockedSocketHandlerConfiguration;
import com.sdggroup.pruebatecnica.model.publisher.PublisherServiceImpl;
import com.sdggroup.pruebatecnica.model.socket.SocketHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = MockedSocketHandlerConfiguration.class)
@ActiveProfiles("test")
public class PublisherServiceTest {

    @Autowired
    private SocketHandler socketHandler;

    @Test
    public void givenValidMsg_whenPublish_thenReturnSent() {

        final PublisherServiceImpl publisherService = new PublisherServiceImpl(socketHandler);

        final SendMessageDto sendMessageDto = new SendMessageDto();
        sendMessageDto.setMsgContent("Test content");

        final MessageSentDto sent = publisherService.publish(sendMessageDto);

        Assertions.assertNotNull(sent);
        Assertions.assertNotNull(sent.getReceivedAt());
        Assertions.assertEquals(sent.getMsgContent(), sendMessageDto.getMsgContent());
    }

    @Test
    public void givenNullMsg_whenPublish_thenException() {

        final PublisherServiceImpl publisherService = new PublisherServiceImpl(socketHandler);

        final SendMessageDto sendMessageDto = new SendMessageDto();

        Assertions.assertThrows(NullMessageException.class, () -> publisherService.publish(null));
        Assertions.assertThrows(NullMessageException.class, () -> publisherService.publish(sendMessageDto));
    }

    @Test
    public void givenEmptyMsg_whenPublish_thenException() {

        final PublisherServiceImpl publisherService = new PublisherServiceImpl(socketHandler);

        final SendMessageDto sendMessageDto = new SendMessageDto();

        sendMessageDto.setMsgContent("");
        Assertions.assertThrows(EmptyMessageException.class, () -> publisherService.publish(sendMessageDto));
    }
}
