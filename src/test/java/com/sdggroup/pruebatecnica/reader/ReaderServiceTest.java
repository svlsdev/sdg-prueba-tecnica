package com.sdggroup.pruebatecnica.reader;

import com.sdggroup.pruebatecnica.api.reader.dto.ReaderDto;
import com.sdggroup.pruebatecnica.mockconfig.MockedSocketHandlerConfiguration;
import com.sdggroup.pruebatecnica.model.reader.ReaderServiceImpl;
import com.sdggroup.pruebatecnica.model.socket.SocketHandler;
import com.sdggroup.pruebatecnica.model.socket.client.SocketClientSessionInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest(classes = MockedSocketHandlerConfiguration.class)
@ActiveProfiles("test")
public class ReaderServiceTest {

    @Autowired
    private SocketHandler socketHandler;

    @Test
    public void givenNonConnectedReaders_whenList_thenReturnEmptyList() {

        Mockito.when(socketHandler.getSocketClientSessionsInfo()).thenReturn(new ArrayList<>());

        final ReaderServiceImpl readerService = new ReaderServiceImpl(socketHandler);
        final List<ReaderDto> list = readerService.list();

        Assertions.assertNotNull(list);
        Assertions.assertTrue(list.isEmpty());
    }

    @Test
    public void givenConnectedReaders_whenList_thenReturnNonEmptyList() {

        final OffsetDateTime now = OffsetDateTime.now();

        final List<SocketClientSessionInfo> sessions = new ArrayList<>();
        sessions.add(new SocketClientSessionInfo("clientId1", now));
        sessions.add(new SocketClientSessionInfo("clientId2", now));

        Mockito.when(socketHandler.getSocketClientSessionsInfo()).thenReturn(sessions);

        final ReaderServiceImpl readerService = new ReaderServiceImpl(socketHandler);
        final List<ReaderDto> list = readerService.list();

        Assertions.assertNotNull(list);
        Assertions.assertFalse(list.isEmpty());
        Assertions.assertEquals(list.size(), 2);

        final ReaderDto reader1 = new ReaderDto();
        reader1.setReaderId("clientId1");
        reader1.setConnectedAt(now);

        Assertions.assertEquals(reader1, list.get(0));
    }
}
