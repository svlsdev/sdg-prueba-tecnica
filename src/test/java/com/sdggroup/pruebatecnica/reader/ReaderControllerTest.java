package com.sdggroup.pruebatecnica.reader;

import com.sdggroup.pruebatecnica.api.reader.ReaderService;
import com.sdggroup.pruebatecnica.api.reader.dto.ReaderDto;
import com.sdggroup.pruebatecnica.mockconfig.MockedReaderServiceConfiguration;
import com.sdggroup.pruebatecnica.rest.reader.response.ReaderListResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = MockedReaderServiceConfiguration.class)
@ActiveProfiles("test")
public class ReaderControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ReaderService readerService;

    @Test
    public void givenNonConnectedReaders_whenList_thenReturnEmptyResponse() {

        Mockito.when(readerService.list()).thenReturn(new ArrayList<>());

        final ReaderListResponse response;
        response = restTemplate.getForObject("/v1/readers", ReaderListResponse.class);

        Assertions.assertNotNull(response);
        Assertions.assertNotNull(response.getReaders());
        Assertions.assertTrue(response.getReaders().isEmpty());
    }

    @Test
    public void givenConnectedReaders_whenList_thenReturnNonEmptyResponse() {

        final List<ReaderDto> readers = new ArrayList<>();

        final ReaderDto reader = new ReaderDto();
        reader.setReaderId("readerId");
        reader.setConnectedAt(OffsetDateTime.now());

        readers.add(reader);

        Mockito.when(readerService.list()).thenReturn(readers);

        final ReaderListResponse response;
        response = restTemplate.getForObject("/v1/readers", ReaderListResponse.class);

        Assertions.assertNotNull(response);
        Assertions.assertNotNull(response.getReaders());
        Assertions.assertFalse(response.getReaders().isEmpty());
        Assertions.assertEquals(response.getReaders().get(0).getReaderId(), readers.get(0).getReaderId());
        Assertions.assertEquals(response.getReaders().get(0).getConnectedAt(), readers.get(0).getConnectedAt().toString());
    }
}
