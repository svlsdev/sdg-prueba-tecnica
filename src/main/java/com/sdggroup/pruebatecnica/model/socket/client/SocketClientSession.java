package com.sdggroup.pruebatecnica.model.socket.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sdggroup.pruebatecnica.model.socket.message.SocketMessage;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * Wrapper to handle a websocket session
 */
public class SocketClientSession {

    private final SocketClientSessionInfo socketClientSessionInfo;

    private final WebSocketSession webSocketSession;

    public SocketClientSession(WebSocketSession webSocketSession) {

        this.webSocketSession = webSocketSession;
        this.socketClientSessionInfo = new SocketClientSessionInfo(webSocketSession.getId(), OffsetDateTime.now());
    }

    public SocketClientSessionInfo getSessionClient() {
        return socketClientSessionInfo;
    }

    public void send(SocketMessage message) throws IOException {

        if (webSocketSession.isOpen()) {

            final ObjectMapper objectMapper = new ObjectMapper();
            final String messageContent = objectMapper.writeValueAsString(message);

            final TextMessage textMessage = new TextMessage(messageContent);

            webSocketSession.sendMessage(textMessage);
        }
    }

    public boolean isOpen() {
        return webSocketSession.isOpen();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SocketClientSession that = (SocketClientSession) o;
        return webSocketSession.equals(that.webSocketSession);
    }

    @Override
    public int hashCode() {
        return Objects.hash(webSocketSession);
    }
}
