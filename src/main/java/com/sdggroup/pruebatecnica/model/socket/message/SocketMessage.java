package com.sdggroup.pruebatecnica.model.socket.message;

import java.util.Objects;

/**
 * Message to be sent through the socket
 */
public class SocketMessage {

    private final String content;

    private final String receivedAt;

    public SocketMessage(String content, String receivedAt) {
        this.content = content;
        this.receivedAt = receivedAt;
    }

    public String getContent() {
        return content;
    }

    public String getReceivedAt() {
        return receivedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SocketMessage that = (SocketMessage) o;
        return Objects.equals(content, that.content) && Objects.equals(receivedAt, that.receivedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(content, receivedAt);
    }

    @Override
    public String toString() {
        return "SocketMessage{" +
                "content='" + content + '\'' +
                ", receivedAt='" + receivedAt + '\'' +
                '}';
    }
}
