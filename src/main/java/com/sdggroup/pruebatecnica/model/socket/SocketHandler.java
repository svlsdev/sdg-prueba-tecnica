package com.sdggroup.pruebatecnica.model.socket;

import com.sdggroup.pruebatecnica.model.socket.client.SocketClientSession;
import com.sdggroup.pruebatecnica.model.socket.client.SocketClientSessionInfo;
import com.sdggroup.pruebatecnica.model.socket.message.SocketMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component("socketHandler")
public class SocketHandler implements WebSocketHandler {

    private static final Logger logger = LoggerFactory.getLogger(SocketHandler.class);

    private final List<SocketClientSession> sessions;

    public SocketHandler() {
        sessions = new ArrayList<>();
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession webSocketSession) {

        final SocketClientSession newSession = new SocketClientSession(webSocketSession);

        sessions.add(newSession);

        logger.info("Socket client session established id={} | clientCount={}", webSocketSession.getId(), sessions.size());
    }

    @Override
    public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) {
        // Messages are only broadcast from REST API
    }

    @Override
    public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) {
        logger.error("Transport error", throwable);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) {

        final SocketClientSession sessionToRemove = new SocketClientSession(webSocketSession);

        sessions.remove(sessionToRemove);

        logger.info("Socket client session closed id={} | clientCount={}", webSocketSession.getId(), sessions.size());
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }

    public void publish(SocketMessage message) {

        logger.info("Broadcasting message to {} connected clients msgLength={}", sessions.size(), message.getContent().length());

        int successCount = 0;
        int failureCount = 0;

        for (SocketClientSession session : sessions) {

            try {

                session.send(message);
                successCount++;

            } catch (IOException e) {
                // Catch to continue sending to the other clients
                logger.error("Error on message send", e);

                failureCount++;
            }
        }

        logger.info("Message broadcast ended sentOK={} | sentKO={}", successCount, failureCount);
    }

    public List<SocketClientSessionInfo> getSocketClientSessionsInfo() {
        return sessions.stream()
                .filter(SocketClientSession::isOpen)
                .map(SocketClientSession::getSessionClient)
                .collect(Collectors.toList());
    }

    /**
     * Purges staled sessions
     */
    @Scheduled(fixedDelay = 30000)
    public void scheduleFixedDelayTask() {

        int prePurgeCount = sessions.size();

        final List<SocketClientSession> closedSessions = sessions.stream()
                .filter(c -> !c.isOpen())
                .collect(Collectors.toList());

        sessions.removeAll(closedSessions);

        int postPurgeCount = sessions.size();

        logger.info("Purged staled sessions staledCount={}", postPurgeCount - prePurgeCount);
    }
}
