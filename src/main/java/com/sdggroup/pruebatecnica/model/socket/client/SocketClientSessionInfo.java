package com.sdggroup.pruebatecnica.model.socket.client;

import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * Info about a socket client session
 */
public class SocketClientSessionInfo {

    private final String clientId;

    private final OffsetDateTime connectedAt;

    public SocketClientSessionInfo(String clientId, OffsetDateTime connectedAt) {
        this.clientId = clientId;
        this.connectedAt = connectedAt;
    }

    public String getClientId() {
        return clientId;
    }

    public OffsetDateTime getConnectedAt() {
        return connectedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SocketClientSessionInfo that = (SocketClientSessionInfo) o;
        return Objects.equals(clientId, that.clientId) && Objects.equals(connectedAt, that.connectedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientId, connectedAt);
    }

    @Override
    public String toString() {
        return "SocketClientSessionInfo{" +
                "clientId='" + clientId + '\'' +
                ", connectedAt=" + connectedAt +
                '}';
    }
}
