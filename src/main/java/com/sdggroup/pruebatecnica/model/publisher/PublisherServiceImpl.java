package com.sdggroup.pruebatecnica.model.publisher;

import com.sdggroup.pruebatecnica.api.publisher.PublisherService;
import com.sdggroup.pruebatecnica.api.publisher.dto.MessageSentDto;
import com.sdggroup.pruebatecnica.api.publisher.dto.SendMessageDto;
import com.sdggroup.pruebatecnica.model.publisher.util.MessageValidationUtil;
import com.sdggroup.pruebatecnica.model.publisher.util.SocketMessageUtil;
import com.sdggroup.pruebatecnica.model.socket.SocketHandler;
import com.sdggroup.pruebatecnica.model.socket.message.SocketMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;

@Component("publisherService")
public class PublisherServiceImpl implements PublisherService {

    private static final Logger logger = LoggerFactory.getLogger(PublisherServiceImpl.class);

    private final SocketHandler socketHandler;

    public PublisherServiceImpl(SocketHandler socketHandler) {
        this.socketHandler = socketHandler;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MessageSentDto publish(SendMessageDto message) {

        MessageValidationUtil.validateOrFail(message);

        logger.info("Publishing new message msgLength={}", message.getMsgContent().length());

        final MessageSentDto sentMsg = new MessageSentDto();
        sentMsg.setMsgContent(message.getMsgContent());
        sentMsg.setReceivedAt(OffsetDateTime.now());

        final SocketMessage socketMessage = SocketMessageUtil.of(sentMsg);
        socketHandler.publish(socketMessage);

        return sentMsg;
    }
}
