package com.sdggroup.pruebatecnica.model.publisher.util;

import com.sdggroup.pruebatecnica.api.publisher.dto.SendMessageDto;
import com.sdggroup.pruebatecnica.api.publisher.exception.EmptyMessageException;
import com.sdggroup.pruebatecnica.api.publisher.exception.NullMessageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for Message validations
 */
public class MessageValidationUtil {

    private static final Logger logger = LoggerFactory.getLogger(MessageValidationUtil.class);

    private MessageValidationUtil() {
    }

    public static void validateOrFail(SendMessageDto sendMessageDto) {

        if (sendMessageDto == null || sendMessageDto.getMsgContent() == null) {

            logger.warn("Received null msg");

            throw new NullMessageException();
        }

        if (sendMessageDto.getMsgContent().trim().isEmpty()) {

            logger.warn("Received empty msg");

            throw new EmptyMessageException();
        }
    }
}
