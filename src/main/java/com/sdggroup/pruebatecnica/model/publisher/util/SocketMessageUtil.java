package com.sdggroup.pruebatecnica.model.publisher.util;

import com.sdggroup.pruebatecnica.api.publisher.dto.MessageSentDto;
import com.sdggroup.pruebatecnica.model.socket.message.SocketMessage;

/**
 * Utility class for mappings targeting SocketMessage
 */
public class SocketMessageUtil {

    private SocketMessageUtil() {

    }

    public static SocketMessage of(MessageSentDto messageSentDto) {

        final String content = messageSentDto.getMsgContent();
        final String receivedAt = messageSentDto.getReceivedAt().toString();

        return new SocketMessage(content, receivedAt);
    }
}
