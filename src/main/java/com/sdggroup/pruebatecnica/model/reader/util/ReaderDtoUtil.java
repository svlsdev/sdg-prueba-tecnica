package com.sdggroup.pruebatecnica.model.reader.util;

import com.sdggroup.pruebatecnica.api.reader.dto.ReaderDto;
import com.sdggroup.pruebatecnica.model.socket.client.SocketClientSessionInfo;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Utility class for mappings targeting ReaderDto
 */
public class ReaderDtoUtil {

    private ReaderDtoUtil() {
    }

    public static ReaderDto of(final SocketClientSessionInfo socketClientSessionInfo) {

        final ReaderDto readerDto = new ReaderDto();
        readerDto.setReaderId(socketClientSessionInfo.getClientId());
        readerDto.setConnectedAt(socketClientSessionInfo.getConnectedAt());

        return readerDto;
    }

    public static List<ReaderDto> of(final List<SocketClientSessionInfo> socketClientSessionInfo) {
        return socketClientSessionInfo.stream().map(ReaderDtoUtil::of).collect(Collectors.toList());
    }
}
