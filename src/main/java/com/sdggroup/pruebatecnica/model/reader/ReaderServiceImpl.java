package com.sdggroup.pruebatecnica.model.reader;

import com.sdggroup.pruebatecnica.api.reader.ReaderService;
import com.sdggroup.pruebatecnica.api.reader.dto.ReaderDto;
import com.sdggroup.pruebatecnica.model.reader.util.ReaderDtoUtil;
import com.sdggroup.pruebatecnica.model.socket.SocketHandler;
import com.sdggroup.pruebatecnica.model.socket.client.SocketClientSessionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("readerService")
public class ReaderServiceImpl implements ReaderService {

    private static final Logger logger = LoggerFactory.getLogger(ReaderServiceImpl.class);

    private final SocketHandler socketHandler;

    public ReaderServiceImpl(SocketHandler socketHandler) {
        this.socketHandler = socketHandler;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ReaderDto> list() {

        logger.info("Retrieving readers list");

        final List<SocketClientSessionInfo> socketClientSessionsInfo = socketHandler.getSocketClientSessionsInfo();

        logger.debug("Readers list {}", socketClientSessionsInfo);

        return ReaderDtoUtil.of(socketClientSessionsInfo);
    }
}
