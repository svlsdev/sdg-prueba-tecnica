package com.sdggroup.pruebatecnica.rest.reader.util;

import com.sdggroup.pruebatecnica.api.reader.dto.ReaderDto;
import com.sdggroup.pruebatecnica.rest.reader.response.ReaderListResponse;
import com.sdggroup.pruebatecnica.rest.reader.response.ReaderResponse;

import java.util.List;

/**
 * Utility class for mappings targeting ReaderListResponse
 */
public class ReaderListResponseUtil {

    private ReaderListResponseUtil() {
    }

    public static ReaderListResponse of(List<ReaderDto> readerDtoList) {

        final ReaderListResponse response = new ReaderListResponse();
        final List<ReaderResponse> readerResponseList = ReaderResponseUtil.of(readerDtoList);

        response.setReaders(readerResponseList);

        return response;
    }
}
