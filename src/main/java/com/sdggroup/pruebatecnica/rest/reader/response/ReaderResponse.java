package com.sdggroup.pruebatecnica.rest.reader.response;

import java.util.Objects;

/**
 * Response or a message reader
 */
public class ReaderResponse {

    private String readerId;

    private String connectedAt;

    public String getReaderId() {
        return readerId;
    }

    public void setReaderId(String readerId) {
        this.readerId = readerId;
    }

    public String getConnectedAt() {
        return connectedAt;
    }

    public void setConnectedAt(String connectedAt) {
        this.connectedAt = connectedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReaderResponse that = (ReaderResponse) o;
        return Objects.equals(readerId, that.readerId) && Objects.equals(connectedAt, that.connectedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(readerId, connectedAt);
    }

    @Override
    public String toString() {
        return "ReaderResponse{" +
                "readerId='" + readerId + '\'' +
                ", connectedAt='" + connectedAt + '\'' +
                '}';
    }
}
