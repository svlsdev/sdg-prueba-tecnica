package com.sdggroup.pruebatecnica.rest.reader;

import com.sdggroup.pruebatecnica.api.reader.ReaderService;
import com.sdggroup.pruebatecnica.api.reader.dto.ReaderDto;
import com.sdggroup.pruebatecnica.rest.reader.response.ReaderListResponse;
import com.sdggroup.pruebatecnica.rest.reader.util.ReaderListResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ReaderController {

    private final ReaderService readerService;

    public ReaderController(ReaderService readerService) {
        this.readerService = readerService;
    }

    @GetMapping("/v1/readers")
    public ResponseEntity<ReaderListResponse> list() {

        final List<ReaderDto> readerList = readerService.list();
        final ReaderListResponse response = ReaderListResponseUtil.of(readerList);

        return ResponseEntity.ok(response);
    }
}
