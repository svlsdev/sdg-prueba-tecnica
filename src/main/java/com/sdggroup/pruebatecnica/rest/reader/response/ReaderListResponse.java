package com.sdggroup.pruebatecnica.rest.reader.response;

import java.util.List;
import java.util.Objects;

/**
 * Response for requested list of readers
 */
public class ReaderListResponse {

    private List<ReaderResponse> readers;

    public List<ReaderResponse> getReaders() {
        return readers;
    }

    public void setReaders(List<ReaderResponse> readers) {
        this.readers = readers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReaderListResponse that = (ReaderListResponse) o;
        return Objects.equals(readers, that.readers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(readers);
    }

    @Override
    public String toString() {
        return "ReaderListResponse{" +
                "readers=" + readers +
                '}';
    }
}
