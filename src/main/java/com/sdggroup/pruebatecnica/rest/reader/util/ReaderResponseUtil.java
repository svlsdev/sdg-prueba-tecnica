package com.sdggroup.pruebatecnica.rest.reader.util;

import com.sdggroup.pruebatecnica.api.reader.dto.ReaderDto;
import com.sdggroup.pruebatecnica.rest.reader.response.ReaderResponse;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Utility class for mappings targeting ReaderResponse
 */
public class ReaderResponseUtil {

    private ReaderResponseUtil() {

    }

    public static ReaderResponse of(ReaderDto readerDto) {

        final ReaderResponse readerResponse = new ReaderResponse();
        readerResponse.setReaderId(readerDto.getReaderId());
        readerResponse.setConnectedAt(readerDto.getConnectedAt().toString());

        return readerResponse;
    }

    public static List<ReaderResponse> of(List<ReaderDto> readerDtoList) {
        return readerDtoList.stream().map(ReaderResponseUtil::of).collect(Collectors.toList());
    }
}
