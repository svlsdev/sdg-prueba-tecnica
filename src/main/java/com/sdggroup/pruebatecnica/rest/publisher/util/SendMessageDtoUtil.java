package com.sdggroup.pruebatecnica.rest.publisher.util;

import com.sdggroup.pruebatecnica.api.publisher.dto.SendMessageDto;
import com.sdggroup.pruebatecnica.rest.publisher.request.SendMessageRequest;

/**
 * Utility class for mappings targeting SendMessageDto
 */
public class SendMessageDtoUtil {

    private SendMessageDtoUtil() {

    }

    public static SendMessageDto of(SendMessageRequest request) {

        final SendMessageDto dto = new SendMessageDto();
        dto.setMsgContent(request.getMsgContent());

        return dto;
    }
}
