package com.sdggroup.pruebatecnica.rest.publisher.request;

import java.util.Objects;

/**
 * Request to send a message
 */
public class SendMessageRequest {

    private String msgContent;

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SendMessageRequest that = (SendMessageRequest) o;
        return Objects.equals(msgContent, that.msgContent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(msgContent);
    }

    @Override
    public String toString() {
        return "SendMessageRequest{" +
                "msgContent='" + msgContent + '\'' +
                '}';
    }
}
