package com.sdggroup.pruebatecnica.rest.publisher;

import com.sdggroup.pruebatecnica.api.publisher.PublisherService;
import com.sdggroup.pruebatecnica.api.publisher.dto.MessageSentDto;
import com.sdggroup.pruebatecnica.api.publisher.dto.SendMessageDto;
import com.sdggroup.pruebatecnica.rest.publisher.request.SendMessageRequest;
import com.sdggroup.pruebatecnica.rest.publisher.response.SendMessageResponse;
import com.sdggroup.pruebatecnica.rest.publisher.util.SendMessageDtoUtil;
import com.sdggroup.pruebatecnica.rest.publisher.util.SendMessageResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PublisherController {

    private final PublisherService publisherService;

    public PublisherController(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @PostMapping("/v1/messages")
    public ResponseEntity<SendMessageResponse> publish(@RequestBody SendMessageRequest request) {

        final SendMessageDto sendMessageDto = SendMessageDtoUtil.of(request);
        final MessageSentDto messageSentDto = publisherService.publish(sendMessageDto);

        final SendMessageResponse response = SendMessageResponseUtil.of(messageSentDto);

        return ResponseEntity.ok(response);
    }

}
