package com.sdggroup.pruebatecnica.rest.publisher.util;

import com.sdggroup.pruebatecnica.api.publisher.dto.MessageSentDto;
import com.sdggroup.pruebatecnica.rest.publisher.response.SendMessageResponse;

/**
 * Utility class for mappings targeting SendMessageResponse
 */
public class SendMessageResponseUtil {

    private SendMessageResponseUtil() {
    }

    public static SendMessageResponse of(MessageSentDto messageSentDto) {

        final SendMessageResponse response = new SendMessageResponse();
        response.setMsgContent(messageSentDto.getMsgContent());
        response.setReceivedAt(messageSentDto.getReceivedAt().toString());

        return response;
    }
}
