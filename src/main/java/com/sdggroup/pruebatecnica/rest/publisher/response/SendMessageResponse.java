package com.sdggroup.pruebatecnica.rest.publisher.response;

import java.util.Objects;

/**
 * Response for a message send request
 */
public class SendMessageResponse {

    private String msgContent;

    private String receivedAt;

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public String getReceivedAt() {
        return receivedAt;
    }

    public void setReceivedAt(String receivedAt) {
        this.receivedAt = receivedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SendMessageResponse that = (SendMessageResponse) o;
        return Objects.equals(msgContent, that.msgContent) && Objects.equals(receivedAt, that.receivedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(msgContent, receivedAt);
    }

    @Override
    public String toString() {
        return "SendMessageResponse{" +
                "msgContent='" + msgContent + '\'' +
                ", receivedAt='" + receivedAt + '\'' +
                '}';
    }
}
