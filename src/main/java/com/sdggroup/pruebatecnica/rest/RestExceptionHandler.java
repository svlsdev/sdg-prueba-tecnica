package com.sdggroup.pruebatecnica.rest;

import com.sdggroup.pruebatecnica.api.publisher.exception.EmptyMessageException;
import com.sdggroup.pruebatecnica.api.publisher.exception.NullMessageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
public class RestExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(NullMessageException.class)
    public APIErrorResponse handleNullMsg(HttpServletResponse response, NullMessageException ex) {

        int status = HttpServletResponse.SC_BAD_REQUEST;
        response.setStatus(status);

        logger.warn("Request failure due to {}", ex.getClass().getCanonicalName());

        return new APIErrorResponse(status, "Message cannot be null");
    }

    @ExceptionHandler(EmptyMessageException.class)
    public APIErrorResponse handleEmptyMsg(HttpServletResponse response, EmptyMessageException ex) {

        int status = HttpServletResponse.SC_BAD_REQUEST;
        response.setStatus(status);

        logger.warn("Request failure due to {}", ex.getClass().getCanonicalName());

        return new APIErrorResponse(status, "Message cannot be empty");
    }

    @ExceptionHandler(Exception.class)
    public APIErrorResponse handleUnexpected(HttpServletResponse response, Exception ex) {

        int status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        response.setStatus(status);

        logger.error("Request failure due to internal error", ex);

        return new APIErrorResponse(status, "Unexpected error");
    }
}
