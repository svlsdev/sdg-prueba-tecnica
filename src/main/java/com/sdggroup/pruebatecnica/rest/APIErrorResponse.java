package com.sdggroup.pruebatecnica.rest;

import java.util.Objects;

public class APIErrorResponse {

    private int httpStatus;

    private String errorMsg;

    public APIErrorResponse(int httpStatus, String errorMsg) {
        this.httpStatus = httpStatus;
        this.errorMsg = errorMsg;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        APIErrorResponse that = (APIErrorResponse) o;
        return httpStatus == that.httpStatus && Objects.equals(errorMsg, that.errorMsg);
    }

    @Override
    public int hashCode() {
        return Objects.hash(httpStatus, errorMsg);
    }

    @Override
    public String toString() {
        return "APIErrorResponse{" +
                "httpStatus=" + httpStatus +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
