package com.sdggroup.pruebatecnica.api.publisher.exception;

/**
 * Exception when trying to send an empty message
 */
public class EmptyMessageException extends RuntimeException {
}
