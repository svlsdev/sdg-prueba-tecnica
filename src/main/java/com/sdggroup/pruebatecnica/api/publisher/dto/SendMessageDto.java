package com.sdggroup.pruebatecnica.api.publisher.dto;

import java.util.Objects;

/**
 * Message to be sent
 */
public class SendMessageDto {

    private String msgContent;

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SendMessageDto that = (SendMessageDto) o;
        return Objects.equals(msgContent, that.msgContent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(msgContent);
    }

    @Override
    public String toString() {
        return "SendMessageDto{" +
                "msgContent='" + msgContent + '\'' +
                '}';
    }
}
