package com.sdggroup.pruebatecnica.api.publisher.dto;

import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * Messages successfully sent
 */
public class MessageSentDto {

    private String msgContent;

    private OffsetDateTime receivedAt;

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public OffsetDateTime getReceivedAt() {
        return receivedAt;
    }

    public void setReceivedAt(OffsetDateTime receivedAt) {
        this.receivedAt = receivedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageSentDto that = (MessageSentDto) o;
        return Objects.equals(msgContent, that.msgContent) && Objects.equals(receivedAt, that.receivedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(msgContent, receivedAt);
    }

    @Override
    public String toString() {
        return "MessageSentDto{" +
                "msgContent='" + msgContent + '\'' +
                ", receivedAt=" + receivedAt +
                '}';
    }
}
