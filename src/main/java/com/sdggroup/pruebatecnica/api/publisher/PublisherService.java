package com.sdggroup.pruebatecnica.api.publisher;

import com.sdggroup.pruebatecnica.api.publisher.dto.MessageSentDto;
import com.sdggroup.pruebatecnica.api.publisher.dto.SendMessageDto;

public interface PublisherService {

    /**
     * Publishes a new message sending it to all connected message readers
     *
     * @param message Message to send
     * @return Messaged sent
     */
    MessageSentDto publish(SendMessageDto message);
}
