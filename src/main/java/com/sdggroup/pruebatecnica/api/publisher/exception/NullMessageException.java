package com.sdggroup.pruebatecnica.api.publisher.exception;

/**
 * Exception when trying to send a null message
 */
public class NullMessageException extends RuntimeException {
}
