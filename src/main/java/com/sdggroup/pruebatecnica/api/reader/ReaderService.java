package com.sdggroup.pruebatecnica.api.reader;

import com.sdggroup.pruebatecnica.api.reader.dto.ReaderDto;

import java.util.List;

public interface ReaderService {

    /**
     * List of all connected message readers currently connected
     *
     * @return List of connected message readers
     */
    List<ReaderDto> list();
}
