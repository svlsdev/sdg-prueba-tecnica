package com.sdggroup.pruebatecnica.api.reader.dto;

import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * Message reader with connection timestamp
 */
public class ReaderDto {

    private String readerId;

    private OffsetDateTime connectedAt;

    public String getReaderId() {
        return readerId;
    }

    public void setReaderId(String readerId) {
        this.readerId = readerId;
    }

    public OffsetDateTime getConnectedAt() {
        return connectedAt;
    }

    public void setConnectedAt(OffsetDateTime connectedAt) {
        this.connectedAt = connectedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReaderDto readerDto = (ReaderDto) o;
        return Objects.equals(readerId, readerDto.readerId) && Objects.equals(connectedAt, readerDto.connectedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(readerId, connectedAt);
    }

    @Override
    public String toString() {
        return "ReaderDto{" +
                "readerId='" + readerId + '\'' +
                ", connectedAt=" + connectedAt +
                '}';
    }
}
