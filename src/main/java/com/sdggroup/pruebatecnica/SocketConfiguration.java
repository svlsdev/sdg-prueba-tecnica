package com.sdggroup.pruebatecnica;

import com.sdggroup.pruebatecnica.model.socket.SocketHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
@EnableScheduling
public class SocketConfiguration implements WebSocketConfigurer {

    private static final String MESSAGES_CHANNEL = "/messages";

    private final SocketHandler socketHandler;

    public SocketConfiguration(SocketHandler socketHandler) {
        this.socketHandler = socketHandler;
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {

        // As this is a demo, socket is enabled for all origins.
        // Careful with the copy paste into production.

        webSocketHandlerRegistry.addHandler(socketHandler, MESSAGES_CHANNEL).setAllowedOrigins("*");
    }
}
